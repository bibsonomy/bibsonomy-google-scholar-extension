var converter = new showdown.Converter();
function updateDescription(markdown) {
	document.querySelector(".description .parsed").innerHTML = 
		converter.makeHtml(markdown);
}

$(document).on("keydown", function(e) {
	console.log(e)
	if (!$(":focus").val()) {
		if (e.key == "ArrowRight") {
			$('.nav-tabs .active + li a').tab('show');
		} else if (!$(".search-input input").val() && e.key == "ArrowLeft") {
			var a = $('.nav-tabs .active').prev().find("a")
			if (a.attr("href") != "#") {
				a.tab('show');
			}
		}
	}
});

chrome.tabs.query({active:true,currentWindow:true}, function(tabs) {
	
	// select tab
	tab = tabs[0]
	
	// common functionality
	document.querySelector('.tags label').addEventListener(
		"click", 
		function(e) {
			console.log("copy:" + $("input.tags").tagsinput('items'))
			tags = $("input.tags").tagsinput('items')
			if(e.shiftKey) {
				tagsString = tags.join(" ")
			} else {
				tagsString = tags.join(",")
			}
			chrome.runtime.sendMessage({
				type: 'copyToClipboard',
				text: tagsString
			});
		});

	// switch mode
	chrome.runtime.sendMessage({tabId: tab.id, url: tab.url, type: "getBookmark"}, function(post) {
		
		console.log(post);
		
		if (post) { // bookmark exists
			
			document.querySelector("#content").style.backgroundColor = "rgba(0, 102, 153, 0.0980392)";
			
			// update controls
			document.querySelector(".onbib").style.display = "inline-block";
			document.querySelector(".onbib a").href = "https://bibsonomy.org/editBookmark?intraHashToUpdate=" + post.bookmark.intrahash;
			document.querySelector(".button.submit").setAttribute("value", "Update");
			document.querySelector(".button.submit").style.display = 'inline-block';
			document.querySelector(".button.delete").style.display = 'inline-block';

			// update bookmark fields
			document.querySelector("input.title").value = post.bookmark.title;
			document.querySelector('input.url').value = tab.url;

			
			if (post.description) {
				updateDescription(post.description);
			} else {
				document.querySelector(".description textarea").style.display = "inline-block";
			}
			document.querySelector(".description .edit").addEventListener(
				"click", 
				function(e) {
					document.querySelector(".description textarea").style.display = "inline-block";
					document.querySelector(".description textarea").select();
					if (post.description) {
						document.querySelector(".description textarea").value = post.description;
					}
					return true;
				});
			
			document.getElementById('postform').addEventListener(
				"submit", 
				function(e) {
					console.log("update")
					e.preventDefault();
					message = {
						type: "updateBookmark",
						tabId: tab.id,
						url: document.querySelector('input.url').value,
						title: document.querySelector('input.title').value,
						description: document.querySelector('.description textarea').value,
						tags: $("input.tags").tagsinput('items')
					}

					// hide controls
					loading(true);
					
					chrome.runtime.sendMessage(message, function(response) {
						console.log(response);
						location.reload();
					});
					return true;
				});
			
			document.querySelector('.button.delete').addEventListener(
				"click", 
				function(e) {
					console.log("delete")
					e.preventDefault();
					message = {
						type: "deleteBookmark",
						tabId: tab.id,
						url: tab.url
					}

					// hide controls
					loading(true);
					
					chrome.runtime.sendMessage(message, function(response) {
						console.log(response);
						location.reload();
					});
					return true;
				});
				
			
		} else { // bookmark does not exist

			// set infos
			document.querySelector('input.title').value = tab.title;
			document.querySelector('input.url').value = tab.url;
			
			// update controls
			document.querySelector(".description textarea").style.display = 'inline-block';
			document.querySelector(".button.submit").value = "Post Bookmark";
			document.querySelector(".button.submit").style.display = 'inline-block';
			focusOnClick(".description .edit", "textarea.description")
			
			document.getElementById('postform').addEventListener(
				"submit", 
				function(e) {
					console.log("post")
					console.log("Request posting bookmark: " + tab.url);
					e.preventDefault();
					message = {
						type: "postBookmark",
						tabId: tab.id,
						url: document.querySelector('input.url').value,
						title: document.querySelector('input.title').value,
						description: document.querySelector('.description textarea').value,
						tags: $("input.tags").tagsinput('items')
					}

					// hide controls
					loading(true);
					
					chrome.runtime.sendMessage(message, function(response) {
						console.log(response);
						location.reload();
					});
					return true;
				});
		}
		
		// display controls
		loading(false);
		
		// add focus
		focusOnClick(".title .edit", "input.title")
		focusOnClick(".url .edit", "input.url")
		focusOnClick(".tags .edit", ".tt-input")
//		focusOnClick(".tags .edit", "input.tags")
		
		init(post);

		
	});
	
	document.getElementById('settingsform').addEventListener(
			"submit", 
			function(e) {
				console.log("updateSettings");
				message = {
					type: "updateSettings",
					user: document.querySelector('input.user').value,
					apikey: document.querySelector('input.apikey').value
				}
				chrome.runtime.sendMessage(message, function(response) {
					console.log(response);
				});
//				alert("User and apikey set.");
				return true;
			});
	
	function focusOnClick(clickSelector, focusSelector) {
		document.querySelector(clickSelector).addEventListener(
			"click", 
			function(e) {
				document.querySelector(focusSelector).select();
				return true;
			});
	}
	
	function loading(loading) {
		if (loading) {
			document.querySelector(".loading").style.display = 'block';
			document.getElementById("postform").style.display = 'none';
		} else {
			document.querySelector(".loading").style.display = 'none';
			document.getElementById("postform").style.display = 'inline-block';
		}
	}

	function init(post) {
		
		// retrieve user settings
		chrome.runtime.sendMessage({type: "retrieveSettings"}, function(retrieve) {
			
			console.log("Retrieved user details")
			console.log(retrieve)
			
			var user = retrieve.user
			var apikey = retrieve.apikey
			
			if(user) {
				document.getElementById("user").value = user;
			}
			
			if(apikey) {
				document.getElementById("apikey").value = apikey;
			}

			if (!user || !apikey) {
				$('.nav.nav-tabs .settings a').tab('show')
				$('.nav.nav-tabs li').toggleClass('disabled', true)
				$(".disabled a").click(function (e) {
					e.preventDefault();
					return false;
				});
				$('#settings .alert').toggleClass('hide', false)
			} else {
				
				// init autocompletion of tags
				chrome.runtime.sendMessage({type: "getTagUrl", reload: true}, function(tagurl) {
					
					var tags = new Bloodhound({
						datumTokenizer : Bloodhound.tokenizers.obj.whitespace('name'),
						queryTokenizer : Bloodhound.tokenizers.whitespace,
							remote: {
								url: tagurl,
								replace : function(url, query) {
									return url + "/" + query;
								},
								filter: function(data) {
									return data.items.map(function(i) { return {
										name: i.label, 
										display: i.label + " (" + i.count + ")",
										count: i.count} });
								}
							}
					});
					tags.initialize();
			
					$("input.tags").tagsinput({
						typeaheadjs: {
							name: 'tags',
							displayKey: 'name',
							valueKey: 'name',
							source: tags.ttAdapter(),
							templates: {
								empty: [
								].join("\n"),
								suggestion: function(d) { return "<div>" + d.name + "<span style='float: right'>(" + d.count + ")<span></div>"; },
							}
						},
						confirmKeys: [32, 44]
					});
					
					$("input.tags").tagsinput("focus")
					
					$("input.tags").tagsinput("input").bind("typeahead:asyncrequest", function() {
						$(".tags.edit").hide()
						$(".tags.loading").show()
						
					});
					$("input.tags").tagsinput("input").bind("typeahead:asynccancel", function() {
						$(".tags.edit").show()
						$(".tags.loading").hide()
					});
					$("input.tags").tagsinput("input").bind("typeahead:asyncreceive", function() {
						$(".tags.edit").show()
						$(".tags.loading").hide()
					});
					// bad hack to reposition the autocomplete menu ... only alternative would probably be to modify typeahead.js lib!?
					$("input.tags").tagsinput("input").bind("typeahead:asyncrequest", function() {
						console.log("open")
						$(".twitter-typeahead").css("position", "static");
						position = $(".bootstrap-tagsinput").position();
						$(".tt-menu").css("top", position.top + $(".bootstrap-tagsinput").outerHeight(true));
						$(".tt-menu").css("left", position.left);
					});
					
					// add tags TODO: restructure this whole file ... this seems the wrong place to add existing tags
					if (post != null && post.tag) {
						post.tag.forEach(function(t) {
							$("input.tags").tagsinput("add", t.name);
						});
					}
					
					
				});
				
				// init MarkDown description rendering
				
				document.querySelector(".description textarea").addEventListener(
					"input", 
					function(e) {
						updateDescription(this.value);
						return true;
					});
				
				// init reset button 
				document.querySelector(".reset").addEventListener(
					"click", 
					function(e) {
						loading(true);
						chrome.runtime.sendMessage({type: "getUser", reload: false}, function(userdetails) {
							console.log("Reset:")
							console.log(userdetails)
							location.reload();
						});
						return true;
					});
				
				// init search
				function bookmarkTemplate(post) {
					return [
						'<div class="bookmark">',
							'<div class="title"><a target="_blank" href="'+post.bookmark.url+'">'+post.bookmark.title+'</a></div>',
							'<div class="url">'+post.bookmark.url+'</div>',
		//					'<div class="tags">'+tags+'</div>',
							'<div class="info"><span class="link"><a target="_blank" href="https://www.bibsonomy.org/editBookmark?intraHashToUpdate='+post.bookmark.intrahash+'" title="Edit on BibSonomy"><img src="res/icon.png"></a></span><div class="posted"><span class="label">Posted</span>: '+post.postingdate+'</div></div>',
						'</div>' ].join("");
				}
				searchTimeout = null;
				$(".search-input input").on("input", function(e) {
					
					if (searchTimeout != null) {
						clearTimeout(searchTimeout)
					}
					
					$("#search .loading").show();
					$("#search .bookmarks").hide();
					
					searchTimeout = setTimeout(function() {
						query = $(".search-input input").val()
						console.log("Requesting bookmarks: " + query)
						$(".bookmarks").empty();
						chrome.runtime.sendMessage(
							{
								type: "searchBookmarks",
								query: query, 
								search: true
							}, 
							function(bookmarks) {
								console.log("Received bookmarks.");
								console.log(bookmarks);
								
								if (bookmarks && bookmarks.posts && bookmarks.posts.post) {
									bookmarks.posts.post.forEach(function(b) {
										$(".bookmarks").append(bookmarkTemplate(b));
									})
								} else {
									console.log("Error retrieving bookmarks.");
								}
								
								$("#search .loading").hide();
								$("#search .bookmarks").show();
							});
					}, 500);
					
				});
				chrome.runtime.sendMessage({type: "searchBookmarks", query: "", search: true}, function(bookmarks) {
					console.log("Received bookmarks: ");
					console.log(bookmarks);
					
					if (bookmarks) {
						bookmarks.posts.post.forEach(function(b) {
							$(".bookmarks").append(bookmarkTemplate(b));
						})
					} else {
						console.log("Error retrieving bookmarks.");
					}
					
					$("#search .loading").hide();
				});
				
				// init focus
				$('a[data-toggle="tab"]').on(
					'shown.bs.tab',
					function(e) {
						var target = $(e.target).attr("href") // activated tab
						if (target == "#search") {
							$("#search input").focus();
						} else if (target == "#content") {
							$("#content input.tags ").tagsinput("focus");
						}
					});
			}
		});
	}
	
});


