curl -v --user becker:apikey -XPOST "https://www.bibsonomy.org/api/users/becker/posts" --header "Content-Type:application/xml" -d '
<bibsonomy>
  <post description="some important bookmark!">
    <user name="becker"/>
    <tag name="tag1"/>
    <tag name="tag2"/>
    <tag name="tag3"/>
    <group name="public"/>
    <bookmark url="https://www.example.com/" title="An exemplary website"/>
  </post>
</bibsonomy>'


curl --user becker:apikey -XPOST -F "main=@post.xml" -F "bibtex=@test.bib;type=application/bibtex" https://www.biblicious.org/api/users/becker/posts  --trace-ascii /dev/stdout

curl --user becker:apikey -XPOST -F "main=@post.xml" -F "bibtex=@test.marc;type=application/marc" https://www.biblicious.org/api/users/becker/posts  --trace-ascii /dev/stdout

