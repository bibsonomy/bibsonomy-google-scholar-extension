function getBibtex(url, callback) {
	
	var xhr = new XMLHttpRequest();
	xhr.open('GET', url, true);
	
	xhr.onload = function(e) {
		if (this.status === 200) {
			var bibtex = this.responseText;
			callback(e, bibtex);
		} else {
			callback(e, null);
		}
		
	};
	xhr.onerror = function(e) {
		callback(e, null);
	}
	
	xhr.send();
}
