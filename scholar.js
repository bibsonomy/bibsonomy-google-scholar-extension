var user = null;

var res = {
	logo: chrome.extension.getURL("res/icon.png"),
	b_blue: chrome.extension.getURL("res/B_blue.png"),
	b_white: chrome.extension.getURL("res/B_white.png"),
	loading: chrome.extension.getURL("res/loading.gif"),
	settings: chrome.extension.getURL("res/settings.png"),
	bibliography: chrome.extension.getURL("res/bibliography.png"),
	settingsIcon: chrome.extension.getURL("res/settings_icon.png"),
	bibliographyRadio: chrome.extension.getURL("res/bibliography_radio.png")
}

// templates

var sessionTagsTemplateString = 
	'<div id="session-tags" class="bibsonomy header">' 
		+ '<div>'
			+ '<span><img class="icon" src="' + res.b_blue + '" /></span>'
			+ '<input class="tags" type="text" value="{{tags}}" />'
			+ '<button id="set-session-tags">Set session tags</button>' 
			+ '<span class="reload control action fa fa-repeat"></span></a>'
		+ '</div>'
//		+ '<div>'
//			+ '<img class="settings" src="' + res.settings + '" />'
//			+ '<img class="settings" src="' + res.bibliography + '" />'
//		+ '</div>'
	+ '</div>';
var sessionTagsTemplate = Handlebars.compile(sessionTagsTemplateString);

var postTemplateString = 
	'<div class="bibsonomy dialog post">' 
		+ '<a href="{{iconUrl}}"><img class="icon" src="' + res.logo + '" /></a>'
		+ '<span class="bibtex button fa fa-book"></span>'
		+ '<form class="post" post="#"><input class="tags" type="text" value="{{tags}}" />'
		+ '<button class="post" type="submit">Post</button></form>'
		+ '<img class="loading hidden" src="' + res.loading + '" />'
		+ '<span class="response success">Posted!</span>'
		+ '<span class="response fail">Failed!</span>'
		+ '<textarea class="bibtex container"></textarea>'
	+ '</div>';
var postTemplate = Handlebars.compile(postTemplateString);

var editTemplateString = 
	'<div class="bibsonomy dialog posted">' 
		+ '<a href="{{iconUrl}}"><img class="icon" src="' + res.logo + '" /></a> '
		+ '<div class="bibtexKey">{{bibtexKey}}</div> '
		+ '<div class="tags">{{#each tags}}<span class="tag">{{this}}</span>{{/each}}</div>'
		+ '{{#if file}}<a href="{{file.hrefUrl}}"><span class="get-file file fa fa-file"></span></a>{{/if}}'
		+ '{{#if addFile}}<span class="add-file file fa fa-file-o"></span>{{/if}}'
		+ '<a href="{{editUrl}}" class="edit"><span class="edit file fa fa-pencil"></span></a>'
		+ '<span class="delete fa fa-times"></span>'
		+ '<img class="loading hidden" src="' + res.loading + '" />'
	+ '</div>';
var editTemplate = Handlebars.compile(editTemplateString);

var messageTemplateString = 
	'<div class="bibsonomy message">' 
	+ '<span><img class="icon" src="' + res.icon + '" /></span>'
		+ '{{#if loading}}<img class="loading" src="' + res.loading + '" />{{/if}}'
		+ '<span class="text">{{message}}</span>'
	+ '</div>';
var messageTemplate = Handlebars.compile(messageTemplateString);

var loginTemplateString = 
	'<div class="bibsonomy header message">'
	+ '<span><img class="icon" src="' + res.b_blue + '" /></span>'
		+ '<span class=text>BibSonomy plug-in is disabled because your are not <a href="http://www.bibsonomy.org/login">logged in</a> to BibSonomy.</span>'
	+ '</div>';
var loginTemplate = Handlebars.compile(loginTemplateString);

var citationSettingsTemplateString = 
	'<div class="bibsonomy header message">'
		+ '<span><a href="https://scholar.google.de/scholar_settings"><img class="icon" src="' + res.b_blue + '" /></a></span>'
		+ '<span class=text>Please go to <a href="https://scholar.google.de/scholar_settings">Google Scholar settings</a> and under <b>"Search results"</b> in the <b>"Bibliography Manager"</b> section, activate <a href="https://scholar.google.de/scholar_settings"><img src="' + res.bibliographyRadio + '"/></a></span>'
//		+ '<div>'
//			+ '<img class="" src="' + res.settings + '" />'
//			+ '<img class="" src="' + res.bibliography + '" />'
//		+ '</div>'
	+ '</div>';
var citationSettingsTemplate = Handlebars.compile(citationSettingsTemplateString);


// functions

function removeMessage(container) {
	var msg = container.querySelector(".bibsonomy.message");
	msg.parentNode.removeChild(msg);
}

function setMessage(container, message, loading, position) {
	
	if (!position) {
		position = 'beforeend';
	}
	
	var html = messageTemplate({message: message, loading: loading});
	container.insertAdjacentHTML(position, html);
}

function sessionTagDialog(sessionTags) {
	
	var html = sessionTagsTemplate({tags: sessionTags.join(" ")});
	document.getElementById("gs_ab").insertAdjacentHTML('afterend', html);
	
	var listener = function(e) {
		var tags = document.querySelector("#session-tags .tags").value
		chrome.runtime.sendMessage({type: "setSessionTags", sessionTags: tags.split("\\s")}, function(response) {
			location.reload();
		});
	};
	document.getElementById("set-session-tags").addEventListener("click", listener, true);
	
	var reloadListener = function(e) {
		chrome.runtime.sendMessage({type: "getUser", reload: true}, function(response) {
			location.reload();
		});
	};
	document.querySelector("#session-tags .reload").addEventListener("click", reloadListener, true);
}

function handleResult(container) {
	
	// remove existing BibSonomy dialogs or messages and reset style
	
	var bib = container.querySelector(".bibsonomy");
	if (bib !== null) {
		bib.parentNode.removeChild(bib);
	}
	container.parentNode.style.backgroundColor = "";
	
	// extract information
	
	var titleLink = container.querySelector(".gs_rt a"); 
	if (titleLink == null) {
		setMessage(container, "No URL to publication available.", false);
		return;
	}
	
	var title = titleLink.text;
	var url = titleLink.getAttribute("href");
	
	// set loading message
	setMessage(container, "Requesting data from BibSonomy.", true);
	
	// handle publication
	var message = {
		type: "handle", 
		title: title, 
		url: url
	}
	
	// request information about the publication from BibSonomy
	chrome.runtime.sendMessage(message, function(response) {

		// remove current message
		removeMessage(container);
		
		console.log(response)
		
		if (response) {
			
			editDialog(container, response);
			
		} else {
			
			postDialog(container, response);
			
		}
	});
}

function getPdfUrl(container) {
	var pdfLabel = container.parentNode.querySelector(".gs_ctg2");
	var pdfAnchor = container.parentNode.querySelector(".gs_ggsd a");
	var pdfUrl = null;
	if (pdfLabel != null && pdfLabel.innerHTML === "[PDF]" && pdfAnchor !== null) {
		pdfUrl = pdfAnchor.getAttribute("href");
	}
	return pdfUrl;
}

function postDialog(container, response) {

	var title = container.querySelector(".gs_rt a").text;
	var pdfLabel = container.querySelector(".gs_rt .gs_ctc")
	var url = container.querySelector(".gs_rt a").getAttribute("href");

	// skip PDFs
//	if (url.indexOf("pdf", url.length - "pdf".length) !== -1 || pdfLabel) {
//		setMessage(container, "Cannot parse publications from plain PDFs.", false);
//		return;
//	}
	
	var iconUrl = "http://www.bibsonomy.org/search/" + encodeURIComponent(title);
	var html = postTemplate({iconUrl: iconUrl, tags: sessionTags + " "});
	container.insertAdjacentHTML('beforeend', html);
	
	if (url.indexOf("pdf", url.length - "pdf".length) !== -1 || pdfLabel) {
		container.querySelector(".bibsonomy").insertAdjacentHTML('beforeend', 
				' <span class="bibtex warning"><span class="fa fa-exclamation-triangle icon"></span> This is a PDF. Please check BibTex carefully!</span>');
	}
	
	// get bibtex link
	var bibtexLink = null;
	var bibtexElement = container.querySelector("[href*=scholar\\.bib]");
	if (bibtexElement != null) {
		bibtexLink = bibtexElement.getAttribute("href");
	}
	
	// register bibtex view
	container.querySelector(".bibtex.button").addEventListener("click", function(e) {
		
		var bibtexContainer = container.querySelector(".bibtex.container");
		
		if (window.getComputedStyle(bibtexContainer).getPropertyValue("display") === "none") {
			
			function showBibtex() {
				container.querySelector(".bibtex.container").style.display = "block";
				container.querySelector(".bibtex.button").style.color = "#069";
				container.querySelector("button.post").style.color = "#069";
			}
			
			// only request bibtex if it has not been requested yet
			if (container.querySelector(".bibtex.container").value.trim() === "") {
				console.log("Requesting bibtex: " + bibtexLink)

				var message = {
					type: "getBibtex",
					bibtexUrl: bibtexLink
				}

				chrome.runtime.sendMessage(message, function(postResponse) {
					console.log(postResponse);
					container.querySelector(".bibtex.container").value = postResponse.bibtex;
					showBibtex();
					
				});

			} else {
				showBibtex();
			}
			
		} else {
			
			container.querySelector(".bibtex.container").style.display = "none";
			container.querySelector(".bibtex.button").style.color = "grey";
			container.querySelector("button.post").style.color = "#444";
			
		}
		
	});
	
	
	// post functionality for button
	container.querySelector("form.post").addEventListener("submit", function(e) {

		// prevent reload
		e.preventDefault();
		
		// extract general information
		
		var titleLink = container.querySelector(".gs_rt a"); 
		var title = titleLink.text;
		var url = titleLink.getAttribute("href");
		
		// get tags
		var tagString = this.parentNode.querySelector("input").value;
		var tags = tagString.split(/\s+/g).
			map(function(t) { return t.trim() }).
			filter(function(t) { return t.length });
		
		// get pdf url
		var pdfUrl = getPdfUrl(container);
		
		// get citation count
		var citations = null;
		var citesElement = container.querySelector("a[href*=cites]");
		if (citesElement != null) {
			citations = citesElement.innerHTML.match(/\d+/)[0];
			tags.push("citedby:scholar:count:" + citations);
			var date = new Date();
			tags.push("citedby:scholar:timestamp:" 
					+ date.getUTCFullYear() + "-" 
					+ (date.getUTCMonth() + 1) + "-" 
					+ date.getUTCDate());
		}
		
		// update style
		container.querySelector(".bibsonomy .loading").style.display = "inline";
		container.querySelector(".bibsonomy button").style.display = "none";
		container.querySelector(".bibsonomy input").setAttribute("readonly", "readonly");
		container.querySelector(".bibsonomy input").style.color = "#aaa";

		// post function
		function postPublication(bibtex) {
		
			// post
			var message = {
				type: "post", 
				url: url,
				bibtexLink: bibtexLink,
				bibtex: bibtex,
				pdf: pdfUrl,
				tags: tags
			}
			
			// post message to background page
			chrome.runtime.sendMessage(message, function(postResponse) {
				
				console.log("Posted:");
				console.log(postResponse);
				
				container.querySelector(".bibsonomy .loading").style.display = "none";
				if (postResponse) {
					container.querySelector(".bibsonomy .success").style.display = "inline";
					handleResult(container, response);
				} else {
					container.querySelector(".bibsonomy .fail").style.display = "inline";
				}
			});
		}
		
		// get bibtex or post modified bibtex
		console.log(container.querySelector(".bibtex.container").value.trim())
		if(container.querySelector(".bibtex.container").value.trim() === "") {

			var message = {
				type: "getBibtex",
				bibtexUrl: bibtexLink
			}

			chrome.runtime.sendMessage(message, function(postResponse) {
				postPublication(postResponse.bibtex);
			});

		} else {
			postPublication(container.querySelector(".bibtex.container").value);
		}
		
	});
		
}

function editDialog(container, publication) {
	
	// set "posted"-indicator for publication container
	
	if (publication.myown) {
		container.parentNode.style.backgroundColor = "rgba(102,204,0,0.1)";
	} else {
		container.parentNode.style.backgroundColor = "rgba(0,102,153,0.1)";
	}
	container.style.padding = "10px 10px 10px 10px";
	
	// get pdf url
	var pdfUrl = getPdfUrl(container);
	console.log(pdfUrl)
	
	// add controls for already posted publication
	
	var showUrl = "http://www.bibsonomy.org/bibtex/" + publication.intrahash + "/" + user;
	
	var editUrl = "http://www.bibsonomy.org/editPublication" // "www." is important so that the referrer work correctly, because bibsonomy.org without the "www" will redirect to www.bibsonomy.org and in the process encode the URL parameters again
		+ "?intraHashToUpdate=" + publication.intrahash
		+ "&referer=" + encodeURIComponent(window.location.href);
	
	console.log(publication)
	var filteredTags = publication.tags.filter(function(t) { return t.indexOf("citedby:") == -1  });
	var templateData = {
		iconUrl: showUrl, 
		bibtexKey: publication.bibtexKey, 
		tags: filteredTags, 
		file: publication.documents[0],
		addFile: (!publication.documents[0] && pdfUrl),
		editUrl: editUrl
	}
	var html = editTemplate(templateData);
	container.insertAdjacentHTML('beforeend', html);
	
	
	
	// set up delete action
	container.querySelector(".bibsonomy .delete").addEventListener("click", function(result) {
		
		container.querySelector(".bibsonomy .loading").style.display = "inline";
		container.querySelector(".bibsonomy .file").style.display = "none";
		container.querySelector(".bibsonomy .delete").style.display = "none";
		container.querySelector(".bibsonomy a.edit").style.display = "none";
		
		// post message to background page
		var message = {
				type: "deletePublication", 
				title: publication.title
		};
		chrome.runtime.sendMessage(message, function(deleteResponse) {
			handleResult(container);
		});
		
	});
	
	// set up upload action
	if (templateData.addFile) {
		container.querySelector(".bibsonomy .add-file").addEventListener("click", function(result) {
			
			container.querySelector(".bibsonomy .loading").style.display = "inline";
			container.querySelector(".bibsonomy .add-file").style.display = "none";
			container.querySelector(".bibsonomy .delete").style.display = "none";
			container.querySelector(".bibsonomy a.edit").style.display = "none";
			
			// post message to background page
			var message = {
					type: "addFile", 
					title: publication.title,
					url: pdfUrl};
			chrome.runtime.sendMessage(message, function(uploadResponse) {
				handleResult(container);
			});
			
		});
	}
}



//execute 

var containers = document.getElementsByClassName("gs_ri");
console.log(containers)

var sessionTags = ""; // TODO: should this be stored as an array?

console.log("Requesting user details.")
chrome.runtime.sendMessage({type: "getUser"}, function(userdetails) {
	
	console.log(userdetails)
	if (userdetails && userdetails.user) {

		var bibtexLinksExist = document.querySelector("[href*=scholar\\.bib]");
		console.log(bibtexLinksExist)
		if (bibtexLinksExist) {
		
			console.log("User details received.");
			console.log(userdetails);
			
			// set user
			user = userdetails.user;
			
			console.log("Requesting session tags.");
			chrome.runtime.sendMessage({type: "getSessionTags"}, function(response) {
	
				console.log("Session tags received.");
				console.log(response);
				
				console.log("Rendering session tags dialog.");
				sessionTags = response;
				sessionTagDialog(sessionTags);
				
				console.log("Handling results.");
				for (var i = 0; i < containers.length; i++) {
					handleResult(containers[i])
				}
			});
		
		} else {
			
			console.log("BibTex citation settings must be activated!")
			var html = citationSettingsTemplate({});
			document.getElementById("gs_ab").insertAdjacentHTML('afterend', html);
		}
		
	} else {
		console.log("No user details found. Please login!")
		var html = loginTemplate({});
		var body = document.getElementById("gs_bdy");
		document.getElementById("gs_ab").insertAdjacentHTML('afterend', html);
	}
});