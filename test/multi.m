--xxxbibsonomy
Content-Disposition: form-data; name="main"; filename="main.xml"
Content-Type: application/xml

<bibsonomy>
  <post>
    <user name="becker"/>
    <group name="public"/>
    <bibtex title="test" year="x" author="x" bibtexKey="x" entrytype="x"/>    
    <publicationFileUpload multipartName="bibtex" />
  </post>
</bibsonomy>
--xxxbibsonomy
Content-Disposition: form-data; name="bibtex; filename="bib.tex"
Content-Type: application/bibtex

@book{dunn1965peabody,
  title={Peabody picture vocabulary test},
  author={Dunn, Lloyd M and Dunn, Leota M and Bulheller, Stephan and H{\"a}cker, Hartmut},
  year={1965},
  publisher={American Guidance Service Circle Pines, MN}
}
--xxxbibsonomy--
