# BibSonomy Scholar

This is a Google Chrome extension tightly integrates BibSonomy into your browser. Currently it features a powerful button for direct bookmarking as well as managing publications on Google Scholar. It can be installed via the [Chrome Web Store](https://chrome.google.com/webstore/detail/bibsonomy-scholar/nfncjdnkilenkgnhchaapiiboaimpoon).

![screen.png](https://bitbucket.org/repo/rr45A4/images/988156058-screen.png)

## Current Features

* BibSonomy button
    * tag autocompletion
    * indicating if the URL has already been posted or not
    * quick bookmark via keyboard shortcut (CTRL+Space)
    * post, update and delete bookmarks with title, tags and description
    * Markdown support for descriptions

* Google Scholar
    * inidicators
        * indicating which publications you have already posted on BibSonomy (blue background)
        * indicating your own posted publications (green background)
    * posting
        * posting and tagging publication without being redirected to BibSonomy (post button)
        * posting automatically tries to add a PDF if it exists
    * editing
        * link for editing posted publication on BibSonomy (pen button)
        * adding PDF to a posted publication (empty document button)
        * deleting a publication (cross button)
    * access
        * link to posted publication on BibSonomy (BibSonomy button)
        * search unknown publication on BibSonomy (BibSonomy button)
        * direct link to posted PDF (filled document button)

## Contact

If you find issues or have suggestions please file a report here:
https://bitbucket.org/bibsonomy/bibsonomy-google-scholar-extension/issues/

### Who to talk to

* Martin Becker (becker@informatik.uni-wuerzburg.de)