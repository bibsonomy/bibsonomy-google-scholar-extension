// TODO: clean up after moving completely to API method calls

function getPublicationInfo(user, interhash, callback) {
	var link = "https://www.bibsonomy.org/bibtex/" + interhash + "/" + user;

	var xhr = new XMLHttpRequest();
	xhr.open('GET', link, true);
	

	
	xhr.onload = function(e) {
		
		var posted = (this.status == "200");

		var post = null;
		var tags = [];
		var myown = false;
		var bibtex = bibtex;
		var intrahash = null;
		var document = null;
		if (posted) {
			
			var parser = new DOMParser();
			var html = parser.parseFromString(this.response, "text/html");
			post = html;
			
			var tagElements = html.querySelectorAll('.tag-list nobr');
			for (var i = 0; i < tagElements.length; i++) {
				var tag = tagElements[i].innerHTML;
				tags.push(tag);
				if (tag == "myown") {
					myown = true;
				}
			}
			
			bibtex = html.querySelector("#citation_bibtex textarea").innerHTML;
			intrahash = bibtex.match(/intrahash\s*=\s*{(.*)},(.*=)?/)[1];
			
			var documentElement = html.querySelector(".bibtexpreviewimage a.pdfFile");
			if (documentElement) {
				document = documentElement.getAttribute("href");
			}
		}
		console.log(tags)
		
		var response = {
				
				posted: posted,
				post: post,
				
				tags: tags,
				myown: myown,
				intrahash: intrahash,
				document: document
		};
		
		callback(response, e);
	};
	xhr.send();
}

function getPostForm(publicationUrl, callback) {

	var postUrl = "https://www.bibsonomy.org/BibtexHandler" +
		"?tags=" +
		"&requTask=upload" +
		"&selection=" +
		"&url=" + encodeURIComponent(publicationUrl);
	
	getPostFormGeneric(postUrl, callback);
}

function getPostFormFromIntrahash(intrahash, callback) {

	var postUrl = "https://www.bibsonomy.org/editPublication?intraHashToUpdate=" + intrahash	
	getPostFormGeneric(postUrl, callback);
}

function getPostFormGeneric(url, callback) {

	var xhr = new XMLHttpRequest();
	xhr.open('GET', url, true);
	
	xhr.onload = function(e) {
		var r = this.response;
		
		var parser = new DOMParser();
		var html = parser.parseFromString(r, "text/html");
		
		var form = html.querySelector('form[id="postForm"]');
		var interhash = form.querySelector('input[name="post.resource.interHash"]').value
		var ckey = form.querySelector('input[name="ckey"]').getAttribute("value");
		
		var response = {
			form: form,
			interhash: interhash,
			ckey: ckey
		}
		
		callback(response, e);
		
	};
	xhr.send();
}

function getBibtexFromBibsonomyScaper(url, callback) {
	var xhr = new XMLHttpRequest();
	xhr.open(
			'POST', 
			'https://scraper.bibsonomy.org/service' +
				'?format=bibtex&selection=&url=' + encodeURIComponent(url));
	xhr.responseType = 'text';
	xhr.onload = function(e) {
		callback(this.response, e)
	}
	xhr.send();
}

function postBibtex(user, apikey, bibtex, tags, callback) {
	
	var main = {
		"post": {
			"bibtex": {
				"author": "x",
				"bibtexKey": "x",
				"entrytype": "x",
				"title": "x",
				"year": "x",
			},
			"group": [{ "name": "public" }],
			"tag": tags.map(function(t) { return { "name": t }; }),
			"user": { "name": user },
			"description": "",
			"publicationFileUpload": {
				"multipartName": "bibtex"
			}
		}
	};
	
	var data = new FormData();
	data.append("main", new Blob([JSON.stringify(main)], { type: "application/json"}));
	data.append("bibtex", new Blob([bibtex], { type: "text/bibtex"}));
			
	var post = new XMLHttpRequest();
	post.open(
			'POST', 
			"https://" + user + ":" + apikey + "@www.bibsonomy.org/api/users/" + user + "/posts", 
			true);
	post.onload = function(e) {
		callback(this, e, data);
	};
	post.send(data);
	
}

function postPublication(url, tags, callback) {
	
	getPostForm(url, function(postForm) {
		
		var element = postForm.form.querySelector('input[name="tags"]')
		if (element) element.parentNode.removeChild(element);

		console.log(postForm.form)
		var authors = postForm.form.ownerDocument.getElementById("post.resource.author").innerHTML.trim().split(/\s+/)[0].replace(/\W/g, '').toLowerCase()
		var year = postForm.form.ownerDocument.getElementById("post.resource.year").getAttribute("value").trim()
		var title = postForm.form.ownerDocument.getElementById("post.resource.title").innerHTML.trim().split(/\s+/)[0].replace(/\W/g, '').toLowerCase()
		
		// set bibtex key
		var bibtexKey = authors + year + title
		postForm.form.ownerDocument.getElementById("post.resource.bibtexKey").setAttribute("value", bibtexKey)
		
		var data = new FormData(postForm.form);
		data.append("tags", tags.join(" "));
		console.log(data)
		
		var post = new XMLHttpRequest();
		post.open(
				'POST', 
				"https://www.bibsonomy.org/editPublication?ckey=" + postForm.ckey, 
				true);
		post.onload = function(e) {
			callback(this, e, postForm);
		};
		post.send(data);
	});

}

function postBookmark(user, apikey, url, title, description, tags, callback) {
	
	var xml = document.implementation.createDocument(null, "bibsonomy");
	
	var postNode = xml.createElement("post");
	postNode.setAttribute("description", description);
	xml.documentElement.appendChild(postNode);
	
	var userNode = xml.createElement("user");
	userNode.setAttribute("name", user);
	postNode.appendChild(userNode);
	
	tags.forEach(function(tag) {
		var tagNode = xml.createElement("tag");
		tagNode.setAttribute("name", tag);
		postNode.appendChild(tagNode);
	});
	
	var bookmarkNode = xml.createElement("bookmark");
	bookmarkNode.setAttribute("url", url);
	bookmarkNode.setAttribute("title", title);
	postNode.appendChild(bookmarkNode);
	
	var post = new XMLHttpRequest();
	post.open(
			'POST', 
			"https://" + user + ":" + apikey + "@www.bibsonomy.org/api/users/" + user + "/posts", 
			true);
	post.onload = function(e) {
		callback(this, e, xml);
	};
	post.send(xml);
}

function updateBookmark(user, apikey, url, title, description, tags, callback) {
	
	var tagsObj = tags.map(function(t) { return { name: t } } )
	
	var obj = {
			post: {
				user: { name: user },
				description: description,
				tag: tagsObj,
				bookmark: {
					url: url,
					title: title
				}
			}
	}
	
	var put = new XMLHttpRequest();
	put.open(
			'PUT', 
			"https://" + user + ":" + apikey + "@www.bibsonomy.org/api/users/" + user + "/posts/" + md5(url) + "?format=json", 
			true);
	put.onload = function(e) {
		callback(this, e, obj);
	};
	put.send(JSON.stringify(obj));
}

function getPublication(user, interhash, callback) {

	var url = "https://" + user + ":" + apikey + "@www.bibsonomy.org" + "/api/posts" +
			"?resourcetype=bibtex" +
			"&resource=" + interhash + 
			"&user=" + user +
			"&format=json";
	
	var xhr = new XMLHttpRequest();
	xhr.open('GET', url, true);
	
	xhr.onload = function(e) {
		var json = JSON.parse(this.response);
		if (json["posts"]["end"] == 0) {
			callback(null)
		} else {
			callback(json["posts"]["post"][0]);
		}
	};
	xhr.send();
}


function getPublicationFromUser(user, apikey, intrahash, callback) {

	var url = "https://" + user + ":" + apikey + "@www.bibsonomy.org" + 
		"/api/users/" + user + "/posts/" + intrahash + "?format=json";
	
	var xhr = new XMLHttpRequest();
	xhr.open('GET', url, true);
	
	xhr.onload = function(e) {
		var json = JSON.parse(this.response)
		callback(json["post"]);
	};
	xhr.send();
}

function parsePost(post) {
	
	var bibtexKey = post["bibtex"]["bibtexKey"];
	var title = post["bibtex"]["title"].trim();
	var intrahash = post["bibtex"]["intrahash"].trim();
	var interhash = post["bibtex"]["interhash"].trim();
	
	var tags = post["tag"].map(function (t) { return t["name"]; });
	var myown = post["tag"].filter(function(t) { return t.name === "myown" }).length > 0;

	var documents = []
	if (post["documents"]) {
		documents = post["documents"]["document"].map(function(d) {
			return {
				filename: d.filename, 
				hrefApi: d.href, 
				hrefUrl: "https://www.bibsonomy.org/documents" +
						"/" + intrahash + 
						"/" + user + 
						"/" + d.filename
			}
		});
	}
	
	var postDetails = {
		bibtexKey: bibtexKey,
		title: title,
		intrahash: intrahash,
		interhash: interhash,
		tags: tags,
		myown: myown,
		documents: documents,
		json: post
	};

	return postDetails;
}

function uploadFileForPublicationApi(user, apikey, url, intrahash, callback) {
	
	var uploadUrl = "https://www.bibsonomy.org" +
			"/api/users/" + user + "/posts/" + intrahash + "/documents/";
			
	var pdfRequest = new XMLHttpRequest();
	pdfRequest.open('GET', url, true);
	pdfRequest.responseType = 'blob'
	pdfRequest.onload = function(e) {
		
		if(this.status === 200) {
		
			var pdf = this.response;
			
			var name = "document.pdf";
			
			var data = new FormData();
			data.append("file", pdf, name);
			data.append("fileId", 1);
			
			var uploadRequest = new XMLHttpRequest();
			uploadRequest.open('POST', uploadUrl, true);
			uploadRequest.setRequestHeader('Authorization', 'Basic ' + btoa(user + ':' + apikey));
			uploadRequest.onload = function (e) {
	
				console.log(this.status);
				console.log(this.response);
				
				var parser = new DOMParser();
				var xml = parser.parseFromString(this.response, "text/xml");
				var filehash = xml.getElementsByTagName("resourcehash")[0].innerHTML;
				var filename = filehash + name
				
				callback(filename)
				
			};
			uploadRequest.send(data);
			
		} else {
			
			callback(false);
		}
		
	};
	pdfRequest.send();
}

function deletePublication(intrahash, ckey, callback) {
	
	var url = "https://www.bibsonomy.org/deletePost" +
			"?resourceHash=" + intrahash +
			"&ckey=" + ckey;
	
	var xhr = new XMLHttpRequest();
	xhr.open('GET', url, true);
	
	xhr.onload = function(e) {
		callback(this.status == 200);
	};
	xhr.send();
}

function deletePublicationApi(user, apikey, intrahash, callback) {
	
	var url = "https://" + user + ":" + apikey + "@www.bibsonomy.org/api/users/" + user + "/posts/" + intrahash;
	
	var xhr = new XMLHttpRequest();
	xhr.open('DELETE', url, true);
	
	xhr.onload = function(e) {
		callback(this.status == 200);
	};
	xhr.send();
}

function getBibtexFromBibsonomyPostDialog(url, callback) {
	var link = "https://www.bibsonomy.org/BibtexHandler" +
		"?tags=dummy" +
		"&requTask=upload" +
		"&selection=" +
		"&url=" + encodeURIComponent(url);

	var xhr = new XMLHttpRequest();
	xhr.open(
			'GET', 
			link, 
			true);
	
	//xhr.responseType = 'text';
	xhr.onload = function(e) {
		var r = this.response;
//		console.log(r);
		
		var parser = new DOMParser();
		var html = parser.parseFromString(r, "text/html");
		var bibtex = html.querySelector('textarea[disabled]').innerHTML;
		
		callback(bibtex, e);
		
	};
	xhr.send();
}

function checkPublication(user, hash, callback) {
	
	var link = "https://www.bibsonomy.org/bibtex/" + hash + "/" + "user";

	var xhr = new XMLHttpRequest();
	xhr.open('GET', link, true);
	
	xhr.onload = function(e) {
		callback(this.status == 200);
	};
	xhr.send();
	
}

function getAllPublications(user, apikey, callback) {
	
	getAllPublicationsRecursive(user, apikey, 0, 1000, 1000, [], callback);
	
	function getAllPublicationsRecursive(user, apikey, start, end, inc, buffer, callback) {
		
		console.log("Requesting publications: [" + start + ", " + end + "]");
		
		var url = "https://" + user + ":" + apikey + "@www.bibsonomy.org/api/posts" +
				"?resourcetype=bibtex" +
				"&start=" + start + 
				"&end=" + end +
				"&user=" + user +
				"&format=json";

		var xhr = new XMLHttpRequest();
		xhr.open('GET', url, true);
		
		xhr.onload = function(e) {

			var json = JSON.parse(this.response);

			console.log(json)
			
			var posts = json["posts"]["post"];
			var allPosts = buffer.concat(posts);

			var currentEnd = json["posts"]["end"];
			console.log("Received publication: " + currentEnd);
			
			if (currentEnd < end) {
				callback(allPosts);
			} else {
				getAllPublicationsRecursive(user, apikey, end, end + inc, inc, allPosts, callback);
			}
			
		};
		xhr.send();
	}
}

function getAllBookmarks(user, apikey, callback) {
	
	getAllPublicationsRecursive(user, apikey, 0, 1000, 1000, [], callback);
	
	function getAllPublicationsRecursive(user, apikey, start, end, inc, buffer, callback) {
		
		console.log("Requesting bookmarks: [" + start + ", " + end + "]");
		
		var url = "https://" + user + ":" + apikey + "@www.bibsonomy.org/api/posts" +
				"?resourcetype=bookmark" +
				"&start=" + start + 
				"&end=" + end +
				"&user=" + user;

		var xhr = new XMLHttpRequest();
		xhr.open('GET', url, true);
		
		xhr.onload = function(e) {

			var parser = new DOMParser();
			var xml = parser.parseFromString(this.response, "application/xml");
			
			console.log(xml)
			
			var posts = xml.getElementsByTagName("post");
			var postsArray = Array.prototype.slice.call( posts );
			var allPosts = buffer.concat(postsArray);

			var postsElement = xml.getElementsByTagName("posts")[0];
			var currentEnd = postsElement.getAttribute("end");
			console.log("Received bookmarks: " + currentEnd);
			
			if (currentEnd < end) {
				callback(allPosts);
			} else {
				getAllPublicationsRecursive(user, apikey, end, end + inc, inc, allPosts, callback);
			}
			
		};
		xhr.send();
	}
}

function searchBookmarks(user, apikey, query, search, callback) {
	
	if (!search) {
		var link = "https://" + user + ":" + apikey + "@www.bibsonomy.org/api/posts?user=" + user + "&resourcetype=bookmark&format=json&tags=" + query.join("+");
	} else {
		var link = "https://" + user + ":" + apikey + "@www.bibsonomy.org/api/posts?user=" + user + "&resourcetype=bookmark&format=json&search=" + query;
	}

	var xhr = new XMLHttpRequest();
	xhr.open('GET', link, true);
	xhr.responseType = "json";

	xhr.onload = function(e) {
		if (this.status == 200) {
			callback(this.response);
		} else {
			callback(false);
		}
	};
	xhr.send();
}


/**
 * Src: https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
 */
function guid() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
	}
	return s4() + s4() 
		+ '-' + s4() 
		+ '-' + s4() 
		+ '-' + s4() 
		+ '-' + s4() + s4() + s4();
}

function normalizeTitle(title) {
	return title.replace(/\W/g, '').toLowerCase();
}


