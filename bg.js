var UPDATE_INTERVAL = 1000 * 60 * 30 // 30 minutes

var lastUpdate = null;

var user = null;
var apikey = null;
var bibsonomy = null;

var publications = null;

var sessionTags = [];

var bookmarkFolder = null;

// register context menu entry to search on Google Scholar
function searchOnScholar(info, tab) {
	console.log("Selected " + info.selectionText);
	chrome.tabs.create({
		url : "https://scholar.google.de/scholar?q=" + info.selectionText,
	});
}
chrome.contextMenus.create({
	title : "Search on Google Scholar",
	contexts : [ "selection" ],
	onclick : searchOnScholar,
});

function searchOnBibsonomyUser(info, tab) {
	console.log("Selected " + info.selectionText);
	chrome.tabs.create({
		url : "https://www.bibsonomy.org/search/user:" + user + " " + info.selectionText,
	});
}
chrome.contextMenus.create({
	title : "Search on BibSonomy (user)",
	contexts : [ "selection" ],
	onclick : searchOnBibsonomyUser,
});



function updateBookmarkStatus(tab) {

	// skip this if the tab is in some intermediate state or not visible
	if (tab.status != "complete" || !tab.active) {
		console.log("Skip update!");
		return;
	}
	
	// set new URL and reset status
	currentUrl = tab.url;
	
	// show page action and load popup
	chrome.pageAction.show(tab.id);
	chrome.pageAction.setPopup({tabId: tab.id, popup: "action.html"});
	
	// check status
	bibsonomy.getUserPost(user,md5(tab.url), function(e, error, bookmark) {
		
		// set status
		console.log("Requested bookmark: ");
		console.log(bookmark)
		
		// set icon according to status
		chrome.pageAction.setIcon(
			{
				tabId: tab.id,
				path: {
					16: bookmark ? "res/B_blue.png" : "res/B_white.png"
				}
			},
			function() {});
		
	});
}

chrome.commands.onCommand.addListener(function(command) {
	console.log('Command:', command);
});

chrome.tabs.onActivated.addListener(function(activeInfo) {
	chrome.tabs.get(activeInfo.tabId, function(tab) {
		console.log("Activated: " + tab.url)
		updateBookmarkStatus(tab);
	});
});

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
	console.log("Updated: " + tab.url);
	updateBookmarkStatus(tab);
});

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	
	console.log("Received message.");
	console.log(sender)
	console.log(request)
	
	if (request.type == "getSessionTags") {
		
		console.log("getSessionTags: " + sessionTags);
		sendResponse(sessionTags);
		
	} else if (request.type == "setSessionTags") {

		console.log("setSessionTags: " + request.sessionTags);
		sessionTags = request.sessionTags;
		sendResponse();
		
	} else if (request.type == "checkPublication") {

		console.log("checkPublication: " + request);
		checkPublication(request.hash, sendResponse);
	
	} else if (request.type == "deletePublication") {
		
		var normalizedTitle = normalizeTitle(request.title)
		var postDetails = publications[normalizedTitle];
		
		deletePublicationApi(user, apikey, postDetails.intrahash, function(success) {
			if (success) {
				delete publications[normalizedTitle];
			}
			sendResponse(success);
		});
		
	} else {
	
		var messageHandler = "messageHandler_" + request.type;
		console.log("received message for: " + messageHandler);
		if (window[messageHandler]) {
			console.log("message handler exists: " + messageHandler);
			window[messageHandler](request, sendResponse);
		} else {
			console.log("Unknown request type: " + request.type);
			sendResponse(null);
		}
	}
	
	// indicate asynchronous handling of the sent message
	return true;
	
});

function messageHandler_handle(request, sendResponse) {
	console.log("Handling result: " + request.title);
	sendResponse(publications[normalizeTitle(request.title)]);
}

function messageHandler_post(request, sendResponse) {

	console.log("Posting publication:");
	console.log(request);
	
	// post bibtex
	bibsonomy.postBibtex(user, request.bibtex, request.tags, function(e, error, intrahash) {
		
		if (!error) {
			
			console.log("Successfully posted publication: " + intrahash);
			console.log("Updating entry.");
			
			// update local cache
			bibsonomy.getUserPost(user, intrahash, function(e, error, post) {
				
				if (!error) {
					
					var postDetails = parsePost(post)
					publications[normalizeTitle(postDetails.title)] = postDetails;
					
					sendResponse(true);
					
				} else {
					console.log("Error posting publication.")
					console.log(error)
					console.log(e)
					sendResponse(false)
				}
				
			});
			
		} else {
			console.log("Error posting publication.")
			console.log(error)
			console.log(e)
			sendResponse(false)
		}
		
	});
	
}

function messageHandler_addFile(request, sendResponse) {

	console.log("AddingFile:");
	console.log(request);
	
	var normalizedTitle = normalizeTitle(request.title)
	var postDetails = publications[normalizedTitle];
	
	getPostFormFromIntrahash(postDetails.intrahash, function(postForm) {
		
		uploadFileForPublicationApi(user, apikey, request.url, postDetails.intrahash, function(success) {
			
			if (success) {
				
				getPublicationFromUser(user, apikey, postDetails.intrahash, function(post) {
					if (post != null) {
						console.log(postDetails)
						var postDetails = parsePost(post);
						publications[normalizedTitle] = postDetails;
						sendResponse(true);
					} else {
						sendResponse(false);
					}
				});
				
			} else {
				sendResponse(false);
			}
		});
	});
	
	// TODO: use API method
//	uploadFileForPublicationApi(user, apikey, request.url, request.intrahash, sendResponse)
	
}

function messageHandler_getUser(request, sendResponse) {
	if (
			publications == null 
			|| new Date().getTime() - lastUpdate > UPDATE_INTERVAL 
			|| request.reload) {
		
		BibSonomy.getUserdetails(function(e, error, userdetails) {
			
			if (userdetails) {
				
				user = userdetails.user
				apikey = userdetails.apikey
				bibsonomy = new BibSonomy(user, apikey)
				
				getAllPublications(user, apikey, function(posts) {

					publications = {}
					
					for (var i = 0; i < posts.length; i++) {
						
						var postDetails = parsePost(posts[i]);
						publications[normalizeTitle(postDetails.title)] = postDetails;
					}
					console.log(publications);

					getAllBookmarks(user, apikey, function(posts) {

						bookmarks = {}
						
//						for (var i = 0; i < posts.length; i++) {
//							
//							var postDetails = parsePost(posts[i]);
//							publications[normalizeTitle(postDetails.title)] = postDetails;
//						}
//						console.log(publications);
	
						lastUpdate = new Date().getTime();
						
						sendResponse(userdetails);
					});
					
//					lastUpdate = new Date().getTime();
//					
//					sendResponse(userdetails);
				});
				
				
			} else {
				sendResponse(null);
			}
			
		});
	} else {
		console.log("Requesting user details skipped because we have it cached.")
		sendResponse({ user: user, apikey: apikey });
	}
}


function messageHandler_postBookmark(request, sendResponse) {

	postBookmark(user, apikey, request.url, request.title, request.description, request.tags, function(success) {
		chrome.tabs.get(request.tabId, function(tab) {
			console.log("Activated: " + tab.url)
			updateBookmarkStatus(tab);
		});
		sendResponse(success)
	});
}

function messageHandler_updateBookmark(request, sendResponse) {
	console.log("received update bookmark request")
	updateBookmark(user, apikey, request.url, request.title, request.description, request.tags, function(success, e, obj) {
		console.log("test")
		console.log(success)
		console.log(e)
		console.log(obj)
		chrome.tabs.get(request.tabId, function(tab) {
			console.log("Activated: " + tab.url)
			updateBookmarkStatus(tab);
		});
		sendResponse(success)
	});
}

function messageHandler_deleteBookmark(request, sendResponse) {
	bibsonomy.deletePost(user, md5(request.url), function(e, error, data) {
		sendResponse(!error);
	});
}

function messageHandler_getBookmark(request, sendResponse) {
	bibsonomy.getUserPost(user, md5(request.url), function(e, error, bookmark) {
		sendResponse(bookmark)
	});
}

function messageHandler_getTagUrl(request, sendResponse) {
	console.log("messageHandler_getTagUrl")
	sendResponse("https://www.bibsonomy.org/json/prefixtags/user/" + user)
}

function messageHandler_searchBookmarks(request, sendResponse) {
	console.log("messageHandler_searchBookmarks")
	searchBookmarks(user, apikey, request.query, request.search, function(bookmarks) {
		sendResponse(bookmarks);
	});
}

function messageHandler_updateSettings(request, sendResponse) {
	console.log("messageHandler_updateSettings");
	apikey = request.apikey;
	user = request.user;
	console.log(user);
	console.log(apikey);
	sendResponse({user: user, apikey: apikey});
}

function messageHandler_retrieveSettings(request, sendResponse) {
	console.log("messageHandler_retrieveSettings");
	if (user == null) {
		user = "";
	}
	if (apikey == null) {
		apikey = "";
	}
	sendResponse({user: user, apikey: apikey});
}

function messageHandler_getBibtex(request, sendResponse) {
	console.log("messageHandler_getBibtex");
	getBibtex(request.bibtexUrl, function(e, bibtex) {
		sendResponse({bibtex: bibtex});
	})

}

// source: https://stackoverflow.com/questions/25622359/clipboard-copy-paste-on-content-script-chrome-extension
function messageHandler_copyToClipboard(request, sendResponse) {
	var input = document.createElement('textarea');
	document.body.appendChild(input);
	input.value = request.text;
	input.focus();
	input.select();
	document.execCommand('Copy');
	input.remove();
}

// initialize context (get user, get password, etc.)
messageHandler_getUser({}, function(response) {});
